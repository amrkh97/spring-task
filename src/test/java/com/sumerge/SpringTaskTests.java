package com.sumerge;

import com.sumerge.Config.JwtUtil;
import com.sumerge.entity.AuthenticationResponse;
import com.sumerge.entity.FoodItemModel;
import com.sumerge.entity.FoodOrderModel;
import com.sumerge.entity.UserModel;
import com.sumerge.repository.FoodItemRepo;
import com.sumerge.repository.FoodOrderRepo;
import com.sumerge.repository.UserRepo;
import com.sumerge.service.FoodItemService;
import com.sumerge.service.FoodOrderService;
import com.sumerge.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.Ordered;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@Log4j2
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class SpringTaskTests {

	@Autowired
	UserRepo userRepo;

	@Autowired
	FoodItemRepo foodItemRepo;

	@Autowired
	FoodOrderRepo foodOrderRepo;


	//----------------------//

	@Autowired
	UserService userService;

	@Autowired
	FoodItemService foodItemService;

	@Autowired
	FoodOrderService foodOrderService;

	@Autowired
	JwtUtil jwtTokenUtil;

	@Autowired
	AuthenticationManager authenticationManager;


	@Test
	@Tag("GET")
	@Order(1)
	@DisplayName("Get All Users")
	void GetAllUsersTest(){

		when(userRepo.findAll()).thenReturn(
				new LinkedList<>(Arrays.asList(
						new UserModel(1, "Amr Khaled", "2015", 2),
						new UserModel(2, "Amr Khaled 2", "2016", 2),
						new UserModel(3, "Amr Khaled 3", "2017", 2),
						new UserModel(4, "Amr Khaled 4", "2018", 2)
				)));

		List<UserModel> expected = userRepo.findAll();
		List<UserModel> actual = userService.getUsers();

		log.info("Test - Get All Students");
		log.info("Expected :" + expected.toString());
		log.info("Actual :" + actual.toString());

		Assertions.assertEquals (expected, actual,
				"User lists should be identical!");
	}

	@Test
	@Tag("POST")
	@Order(2)
	@DisplayName("Add New User")
	void addNewUserTest(){
		UserModel userModel = new UserModel("Amr Khaled","Password");

		when(userRepo.save(any(UserModel.class))).thenReturn(
			userModel
		);

		UserModel expected = userRepo.save(new UserModel());
		UserModel actual = userService.addUser(new UserModel());

		log.info("Test - Add New Student");
		log.info("Expected :" + expected.toString());
		log.info("Actual :" + actual.toString());

		Assertions.assertEquals (expected, actual,
				"Users should be identical!");
	}

	@Test
	@Tag("GET")
	@Order(3)
	@DisplayName("Get All Food Items")
	void GetAllFoodItemsTest(){

		when(foodItemRepo.findAll()).thenReturn(
				new LinkedList<>(Arrays.asList(
						new FoodItemModel("1", "Amr Khaled", 2015, "No Image"),
						new FoodItemModel("2", "Amr Khaled 2", 2016, "No Image 2"),
						new FoodItemModel("3", "Amr Khaled 3", 2017, "No Image 3"),
						new FoodItemModel("4", "Amr Khaled 4", 2018, "No Image 4")
				)));

		List<FoodItemModel> expected = foodItemRepo.findAll();
		List<FoodItemModel> actual = foodItemService.getFoodItems();

		log.info("Test - Get All FoodItems");
		log.info("Expected :" + expected.toString());
		log.info("Actual :" + actual.toString());

		Assertions.assertEquals (expected, actual,
				"Food Item lists should be identical!");
	}


	@Test
	@Tag("POST")
	@Order(4)
	@DisplayName("Add New Food Item")
	void addNewFoodItemsTest(){

		when(foodItemRepo.save(any(FoodItemModel.class))).thenReturn(
				new FoodItemModel("1", "Amr Khaled", 2015, "No Image")
		);

		FoodItemModel expected = foodItemRepo.save(new FoodItemModel());
		FoodItemModel actual = foodItemService.saveFoodItem(new FoodItemModel());

		log.info("Test - Add New FoodItem");
		log.info("Expected :" + expected.toString());
		log.info("Actual :" + actual.toString());

		Assertions.assertEquals (expected, actual,
				"Food Item should be identical!");
	}


	@Test
	@Tag("GET")
	@Order(5)
	@DisplayName("Get Orders By User Id")
	void getFoodOrdersByUserIdTest(){

		FoodItemModel newFoodItem = foodItemService.saveFoodItem(new FoodItemModel());
		List<FoodItemModel> foodList = new LinkedList<>(Arrays.asList(
				newFoodItem
		));

		FoodOrderModel ourFoodOrder = new FoodOrderModel(new UserModel(), foodList);
		List<FoodOrderModel> foodOrder = new LinkedList<>(Arrays.asList(
				ourFoodOrder
		));

		when(foodOrderRepo.findByUserIdId(any(Integer.class))).thenReturn(
				foodOrder
		);

		List<FoodOrderModel> actual = foodOrderService.getOrdersByUserId(2);
		List<FoodOrderModel> expected = foodOrderRepo.findByUserIdId(2);


		log.info("Get Past Orders By User ID");
		log.info("Expected :" + expected.toString());
		log.info("Actual :" + actual.toString());

		Assertions.assertEquals (expected, actual,
				"Food Item should be identical!");
	}

	@Test
	@Tag("GET")
	@Order(6)
	@DisplayName("Get All Food Orders")
	void getAllFoodOrders(){

		FoodItemModel newFoodItem = foodItemService.saveFoodItem(new FoodItemModel());
		List<FoodItemModel> foodList = new LinkedList<>(Arrays.asList(
				newFoodItem
		));

		FoodOrderModel ourFoodOrder = new FoodOrderModel(new UserModel(), foodList);
		List<FoodOrderModel> foodOrder = new LinkedList<>(Arrays.asList(
				ourFoodOrder
		));

		when(foodOrderRepo.findAll()).thenReturn(
				foodOrder
		);

		List<FoodOrderModel> actual = foodOrderService.getAll();
		List<FoodOrderModel> expected = foodOrderRepo.findAll();


		log.info("Get All Food Orders");
		log.info("Expected :" + expected.toString());
		log.info("Actual :" + actual.toString());

		Assertions.assertEquals (expected, actual,
				"Food Order Lists should be identical!");
	}

	@Test
	@Tag("POST")
	@Order(7)
	@DisplayName("Save Multiple Food Items")
	void saveFoodItemsTest(){

		FoodItemModel newFoodItem = foodItemService.saveFoodItem(new FoodItemModel());
		List<FoodItemModel> foodList = new LinkedList<>(Arrays.asList(
				newFoodItem
		));

		when(foodItemRepo.saveAll(any())).thenReturn(
				foodList
		);

		List<FoodItemModel> actual = foodItemService.saveFoodItems(foodList);
		List<FoodItemModel> expected = foodItemRepo.saveAll(foodList);


		log.info("Save Multiple Food Items");
		log.info("Expected :" + expected.toString());
		log.info("Actual :" + actual.toString());

		Assertions.assertEquals (expected, actual,
				"Food Item Lists should be identical!");
	}

	@Test
	@Tag("GET")
	@Order(8)
	@DisplayName("Get Food Item By ID")
	void getFoodItemByIdTest(){


		when(foodItemRepo.existsById(any(Integer.class))).thenReturn(
				true
		);

		Boolean actual = foodItemService.getFoodItem(new FoodItemModel());
		Boolean expected = foodItemRepo.existsById(0);


		log.info("Get Food Item By ID");
		log.info("Expected :" + expected.toString());
		log.info("Actual :" + actual.toString());

		Assertions.assertEquals (expected, actual,
				"Food Order Lists should be identical!");
	}

	@Test
	@Tag("POST")
	@Order(9)
	@DisplayName("Calculate Food Total")
	void calculateFoodTotalTest(){

		List<FoodItemModel> foodItems = new LinkedList<>(Arrays.asList(
				new FoodItemModel("1", "Amr Khaled", 1, "No Image"),
				new FoodItemModel("2", "Amr Khaled 2", 2, "No Image 2"),
				new FoodItemModel("3", "Amr Khaled 3", 3, "No Image 3"),
				new FoodItemModel("4", "Amr Khaled 4", 4, "No Image 4")
		));

		float actual = foodItemService.getOrderTotal(foodItems);
		float expected = 10;


		log.info("Get Total Of Order:");
		log.info("Expected :" + expected);
		log.info("Actual :" + actual);

		Assertions.assertEquals (expected, actual,
				"It should return the correct total!");

	}

	@Test
	@Tag("POST")
	@Order(10)
	@DisplayName("Check Food Items Added To List")
	void checkFoodItemsAddedToList(){

		when(foodItemRepo.existsById(any())).thenReturn(true);

		List<FoodItemModel> foodItems = new LinkedList<>(Arrays.asList(
				new FoodItemModel("1", "Amr Khaled", 1, "No Image"),
				new FoodItemModel("2", "Amr Khaled 2", 2, "No Image 2"),
				new FoodItemModel("3", "Amr Khaled 3", 3, "No Image 3"),
				new FoodItemModel("4", "Amr Khaled 4", 4, "No Image 4")
		));

		boolean actual = foodItemService.checkAddedFoodListItems(foodItems);
		boolean expected = false;


		log.info("Checking If Items Where added successfully");
		log.info("Expected :" + expected);
		log.info("Actual :" + actual);

		Assertions.assertEquals (expected, actual,
				"It should return false!");

	}

	@Test
	@Tag("POST")
	@Order(11)
	@DisplayName("Add New User -Null")
	void addNewUserWithNull(){

		UserModel model = new UserModel("Amr", "Khaled");

		when(userRepo.findByUserName(any())).thenReturn(
				null
		);

		when(userRepo.save(any())).thenReturn(
				new UserModel()
		);


		UserModel actual = userService.addUser(new UserModel());
		UserModel expected = new UserModel();


		log.info("Checking If User Was Added successfully");
		log.info("Expected :" + expected);
		log.info("Actual :" + actual);

		Assertions.assertEquals (expected, actual,
				"It should return false!");

	}

	@Test
	@Tag("POST")
	@Order(12)
	@DisplayName("Load User By Username")
	void loadUserByUserName(){

		UserModel model = new UserModel("Amr", "Khaled");

		when(userRepo.findByUserName(any())).thenReturn(
				model
		);


		UserDetails actual = userService.loadUserByUsername("Amr");
		User expected = new User("Amr",
				"Khaled", new ArrayList<>());


		log.info("Checking If User Was Loaded successfully");
		log.info("Expected :" + expected);
		log.info("Actual :" + actual);

		Assertions.assertEquals (expected, actual,
				"It should return false!");

	}

	@Test
	@Tag("GET")
	@Order(13)
	@DisplayName("Get User By Username")
	void getUserByUserName(){

		UserModel model = new UserModel("Amr", "Khaled");

		when(userRepo.findByUserName(any())).thenReturn(
				model
		);


		UserModel actual = userService.getUserByUserName("Amr");
		UserModel expected = model;


		log.info("Checking If User Was Loaded successfully");
		log.info("Expected :" + expected);
		log.info("Actual :" + actual);

		Assertions.assertEquals (expected, actual,
				"It should return false!");

	}

	@Test
	@Tag("POST")
	@Order(14)
	@DisplayName("Authenticate User")
	void authenticateUserTest() throws Exception {

		UserModel model = new UserModel("Amr", "Khaled");

		when(userRepo.findByUserName(any())).thenReturn(
				model
		);

		UserDetails returnedUserData = userService.loadUserByUsername("Amr");

		AuthenticationResponse actual = userService.authenticateUser(model);
		AuthenticationResponse expected = new AuthenticationResponse(
				jwtTokenUtil.generateToken(returnedUserData)
		);


		log.info("Checking If User Was Loaded successfully");
		log.info("Expected :" + expected);
		log.info("Actual :" + actual);

		Assertions.assertEquals (expected.getJwt(), actual.getJwt(),
				"It should return false!");

	}

	@Test
	@Tag("POST")
	@Order(15)
	@DisplayName("Authenticate User - Exception")
	void authenticateUserThrowsExceptionTest() throws Exception {

		when(authenticationManager.authenticate(any())).thenThrow(BadCredentialsException.class);


		log.info("Checking If exception gets thrown");

		Assertions.assertThrows(
				Exception.class,
				() -> userService.authenticateUser(new UserModel()),
				"Expected authenticateUser() to throw, but it didn't"
		);

	}



	/////////////////////////////////////////////////////////////////////////
	//Integration Test:
	@Test
	@Tag("POST")
	@Order(Ordered.LOWEST_PRECEDENCE)
	@DisplayName("Integration Test - Order A New Food Item")
	void integrateFoodOrderTest(){

		//Project Flow:
		// (1) Adding a new user to be used in testing:
		when(userRepo.save(any(UserModel.class))).thenReturn(
				new UserModel("amrkh97", "asdfasdf")
		);

		UserModel newUser = userService.addUser(new UserModel());


		// (2) Adding a new food item to be used in the cart testing:
		when(foodItemRepo.save(any(FoodItemModel.class))).thenReturn(
				new FoodItemModel("1", "Amr Khaled", 2015, "No Image")
		);

		FoodItemModel newFoodItem = foodItemService.saveFoodItem(new FoodItemModel());
		List<FoodItemModel> foodList = new LinkedList<>(Arrays.asList(
				newFoodItem
		));

		// (3) Creating a new order with our newly created data:
		when(foodOrderRepo.save(any(FoodOrderModel.class))).thenReturn(
				new FoodOrderModel()
		);

		FoodOrderModel ourFoodOrder = new FoodOrderModel(newUser, foodList);

		FoodOrderModel actual = foodOrderService.saveFoodOrder(new FoodOrderModel());
		FoodOrderModel expected = foodOrderRepo.save(new FoodOrderModel());


		log.info("Integration Test - Ordering A newly Added FoodItem");
		log.info("Expected :" + expected.toString());
		log.info("Actual :" + actual.toString());

		Assertions.assertEquals (expected, actual,
				"Food Item should be identical!");
	}



}
