package com.sumerge.RepoTestConfiguration;

import com.sumerge.repository.FoodItemRepo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.mock;

@Profile("test")
@Configuration
public class FoodItemRepoTestConfiguration {
   @Bean
   @Primary
   public FoodItemRepo customFoodItemRepo() {
      return mock(FoodItemRepo.class);
   }

}