package com.sumerge.RepoTestConfiguration;

import com.sumerge.repository.FoodOrderRepo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.mock;

@Profile("test")
@Configuration
public class FoodOrderRepoTestConfiguration {
   @Bean
   @Primary
   public FoodOrderRepo customFoodOrderRepo() {
      return mock(FoodOrderRepo.class);
   }

}