package com.sumerge.RepoTestConfiguration;

import com.sumerge.repository.UserRepo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.mock;

@Profile("test")
@Configuration
public class UserRepoTestConfiguration {
   @Bean
   @Primary
   public UserRepo customUserRepo() {
      return mock(UserRepo.class);
   }

}