package com.sumerge.RepoTestConfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;

import static org.mockito.Mockito.mock;

@Profile("test")
@Configuration
public class AuthenticationManagerTestConfiguration {
   @Bean
   @Primary
   public AuthenticationManager customAuthenticationManager()
   {
      return mock(AuthenticationManager.class);
   }

}