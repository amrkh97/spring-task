package com.sumerge;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Log4j2
public class SpringTask {

	public static void main(String[] args) {
		SpringApplication.run(SpringTask.class, args);
		log.info("Application Started");
	}

}
