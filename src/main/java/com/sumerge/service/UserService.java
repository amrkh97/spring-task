package com.sumerge.service;

import com.sumerge.Config.JwtUtil;
import com.sumerge.entity.AuthenticationResponse;
import com.sumerge.entity.UserModel;
import com.sumerge.repository.UserRepo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Log4j2
public class UserService implements UserDetailsService {
    @Autowired
    private UserRepo repository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;


    public List<UserModel> getUsers() {
        return repository.findAll();
    }

    public UserModel addUser(UserModel userModel){
        UserModel model = repository.findByUserName(userModel.getUserName());
        if(model != null)
            return model;
        else
            return repository.save(userModel);

    }

    public UserDetails loadUserByUsername(String username) {
        UserModel userModel = repository.findByUserName(username);

        return new User(userModel.getUserName(),
                userModel.getPassword(),
                new ArrayList<>());
    }

    public UserModel getUserByUserName(String username){
        return repository.findByUserName(username);
    }

    public AuthenticationResponse authenticateUser(UserModel authenticationRequest) throws Exception {

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(),
                            authenticationRequest.getPassword())
            );
        }
        catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password", e);
        }

        final UserDetails userDetails = this.loadUserByUsername(authenticationRequest.getUserName());

        final String jwt = jwtTokenUtil.generateToken(userDetails);

        return new AuthenticationResponse(jwt);

    }

}
