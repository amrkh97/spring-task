package com.sumerge.service;

import com.sumerge.entity.FoodItemModel;
import com.sumerge.repository.FoodItemRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FoodItemService {

    @Autowired
    private FoodItemRepo repository;

    public FoodItemModel saveFoodItem(FoodItemModel product) {
        return repository.save(product);
    }

    public List<FoodItemModel> getFoodItems() {
        return repository.findAll();
    }

    public boolean getFoodItem(FoodItemModel foodItemModel) {
        return repository.existsById(foodItemModel.getId());
    }

    public List<FoodItemModel> saveFoodItems(List<FoodItemModel> foodItemList) {
        return repository.saveAll(foodItemList);
    }

    public boolean checkAddedFoodListItems(List<FoodItemModel> newFoodListObject) {
        List<Boolean> isAdded = new ArrayList<>();
        newFoodListObject.forEach(foodItemModel -> {
                    isAdded.add(repository.existsById(foodItemModel.getId()));
                });
        // true: There was an error in insertions
        // false: Added successfully
        return isAdded.contains(false);
    }


    public float getOrderTotal(List<FoodItemModel> foodItemList) {
        /*
        float orderTotal = 0;
        List<Float> temp = new ArrayList<Float>();
        foodItemList.forEach((foodItemModel)->{
            temp.add(repository.findById(foodItemModel.getId()).get().getPrice());
        });
         */

        return foodItemList.stream()
                .reduce((a,b)-> new FoodItemModel(
                        "","",a.getPrice() + b.getPrice(),""))
                .get().getPrice();
    }
}
