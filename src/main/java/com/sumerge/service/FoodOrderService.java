package com.sumerge.service;

import com.sumerge.entity.FoodOrderModel;
import com.sumerge.entity.UserModel;
import com.sumerge.repository.FoodOrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FoodOrderService {

    @Autowired
    private FoodOrderRepo repository;

    public FoodOrderModel saveFoodOrder(FoodOrderModel product) {
        return repository.save(product);
    }

    public List<FoodOrderModel> getOrdersByUserId(Integer id){
        return repository.findByUserIdId(id);
    }

    public List<FoodOrderModel> getAll() {
        return repository.findAll();
    }

    //public FoodOrderModel getFoodOrder()
}
