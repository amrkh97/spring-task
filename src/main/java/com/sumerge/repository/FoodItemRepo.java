package com.sumerge.repository;

import com.sumerge.entity.FoodItemModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodItemRepo extends JpaRepository<FoodItemModel,Integer> {
    FoodItemModel findByName(String name);
}
