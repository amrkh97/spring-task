package com.sumerge.repository;

import com.sumerge.entity.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<UserModel,Integer> {
    UserModel findByUserName(String name);
}
