package com.sumerge.repository;

import com.sumerge.entity.FoodOrderModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FoodOrderRepo extends JpaRepository<FoodOrderModel,Integer> {
    Optional<FoodOrderModel> findById(Integer id);

    List<FoodOrderModel> findByUserIdId(Integer id);
}
