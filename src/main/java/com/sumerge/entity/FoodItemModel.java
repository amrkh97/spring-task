package com.sumerge.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Data
@Entity
@Table(name = "FOOD_ITEMS")
public class FoodItemModel {

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String description;
    private String imgPath;
    private float price;

    public FoodItemModel(String name, String desc,float price, String imgPath) {
        this.name = name;
        this.description = desc;
        this.price = price;
        this.imgPath = imgPath;
    }

    public FoodItemModel() {
        this.name = "New Food";
        this.description = "This is a default message";
        this.price = 0;
        this.imgPath = "No Image Provided";
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String desc) {
        this.description = desc;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FoodItemModel)) return false;
        FoodItemModel that = (FoodItemModel) o;
        return id == that.id &&
                Float.compare(that.getPrice(), getPrice()) == 0 &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getDescription(), that.getDescription()) &&
                Objects.equals(getImgPath(), that.getImgPath());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, getName(), getDescription(), getImgPath(), getPrice());
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", imgPath='" + imgPath + '\'' +
                ", price=" + price +
                '}';
    }
}
