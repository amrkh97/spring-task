package com.sumerge.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "food_order")
public class FoodOrderModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name="userId", referencedColumnName="id")
    private UserModel userId;

    @ManyToMany(cascade = {CascadeType.MERGE})
    private List<FoodItemModel> foodOrder;

    private float total;

    public FoodOrderModel() {
    }

    public FoodOrderModel(UserModel userId, List<FoodItemModel> foodOrder) {
        this.userId = userId;
        this.foodOrder = foodOrder;
        this.total = (float) this.foodOrder.stream()
                .mapToDouble(FoodItemModel::getPrice)
                .sum();
    }

    public UserModel getUserId() {
        return userId;
    }

    public void setUserId(UserModel userId) {
        this.userId = userId;
    }

    public List<FoodItemModel> getFoodOrder() {
        return foodOrder;
    }

    public void setFoodOrder(List<FoodItemModel> foodOrder) {
        this.foodOrder = foodOrder;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "FoodOrderModel{" +
                "id=" + id +
                ", userId=" + userId +
                ", foodOrder=" + foodOrder +
                ", total=" + total +
                '}';
    }
}
