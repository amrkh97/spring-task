package com.sumerge.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Users")
public class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "user_name", unique=true)
    private String userName;
    @Column(name = "password")
    private String password;

    private int position;


    public UserModel(String userName, String password) {
        this.userName = userName;
        this.password = password;
        this.position = 2;

    }

    public UserModel(){
        this.userName = "johnDoe";
        this.password = "Password";
        this.position = 2;
    }

    public UserModel(int id, String userName, String password, Integer position) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.position = position;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", position=" + position +
                '}';
    }
}
