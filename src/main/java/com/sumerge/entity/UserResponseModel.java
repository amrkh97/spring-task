package com.sumerge.entity;

public class UserResponseModel {

    UserModel userModel;
    String jwt;

    public UserResponseModel(UserModel userModel, String jwt) {
        this.userModel = userModel;
        this.jwt = jwt;
    }

    public UserResponseModel() {
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    @Override
    public String toString() {
        return "UserResponseModel{" +
                "userModel=" + userModel +
                ", jwt='" + jwt + '\'' +
                '}';
    }
}
