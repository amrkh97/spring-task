package com.sumerge.controller;

import com.sumerge.entity.FoodItemModel;
import com.sumerge.entity.ResponseModel;
import com.sumerge.service.FoodItemService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Log4j2
public class FoodItemController {

    @Autowired
    private FoodItemService service;

    @PostMapping("/foodItems/add")
    public ResponseEntity<String> addFoodItem(@RequestBody FoodItemModel foodItemModel) {

        FoodItemModel newFoodObject = service.saveFoodItem(foodItemModel);
        if (!service.getFoodItem(newFoodObject)) {
            return ResponseEntity.badRequest()
                    .body("Error in adding food item: " + newFoodObject.toString());
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body("Food item added successfully! with id: " + newFoodObject.getId() );
    }

    @GetMapping("/foodItems")
    public List<FoodItemModel> findAllFoodItems() {
        return service.getFoodItems();
    }

    @PostMapping("/foodItems/add/list")
    public ResponseEntity<ResponseModel> addFoodItemsList(@RequestBody List<FoodItemModel> foodItemList) {

        float orderTotal = service.getOrderTotal(foodItemList);
        List<FoodItemModel> newFoodListObject = service.saveFoodItems(foodItemList);

        if (service.checkAddedFoodListItems(newFoodListObject)) {
            return ResponseEntity.badRequest()
                    .body(new ResponseModel(400,
                            "Error in adding food items"));
        }

        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseModel(200,
                "Food Items added successfully"));
    }

}
