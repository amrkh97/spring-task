package com.sumerge.controller;

import com.sumerge.entity.UserModel;
import com.sumerge.entity.UserResponseModel;
import com.sumerge.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Log4j2
public class UserController {
    @Autowired
    private UserService service;


    @GetMapping("/users")
    public List<UserModel> findAllUsers() {
        return service.getUsers();
    }

    @PostMapping("/users/add")
    public ResponseEntity<UserResponseModel> addUser(@RequestBody UserModel userModel) {

        UserModel requestResponse = null;
        String userToken = "";
        try {
            requestResponse = service.addUser(userModel);
            userToken = service.authenticateUser(userModel).getJwt();
        }catch (Exception e){
            return ResponseEntity.badRequest()
                    .body(new UserResponseModel());
        }

        return ResponseEntity.ok()
                .body(new UserResponseModel(requestResponse,userToken));
    }

    @PostMapping("/users/login")
    public ResponseEntity<UserResponseModel> loginUser(@RequestBody UserModel userModel) {

        UserModel requestResponse = null;
        String userToken = "";
        try {
            requestResponse = service.getUserByUserName(userModel.getUserName());
            userToken = service.authenticateUser(userModel).getJwt();

        }catch (Exception e){
            return ResponseEntity.badRequest()
                    .body(new UserResponseModel());
        }

        return ResponseEntity.ok()
                .body(new UserResponseModel(requestResponse,userToken));
    }


    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody UserModel authenticationRequest)
            throws Exception {

        return ResponseEntity.ok(service.authenticateUser(authenticationRequest));
    }


}
