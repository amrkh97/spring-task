package com.sumerge.controller;

import com.sumerge.entity.FoodItemModel;
import com.sumerge.entity.FoodOrderModel;
import com.sumerge.service.FoodOrderService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Log4j2
public class FoodOrderController {

    @Autowired
    private FoodOrderService service;

    @GetMapping("/foodOrders/user/{id}")
    public List<FoodOrderModel> findAllFoodOrdersByUserId(@PathVariable Integer id){

        return service.getOrdersByUserId(id);
    }

    @PostMapping("/foodOrders/add")
    public FoodOrderModel addFoodOrder(@RequestBody FoodOrderModel foodOrderModel) {

        foodOrderModel.setTotal(
                (float) foodOrderModel.getFoodOrder()
                        .stream().mapToDouble(FoodItemModel::getPrice)
                        .sum());

        return service.saveFoodOrder(foodOrderModel);
    }

    @GetMapping("/foodOrders")
    public List<FoodOrderModel> getAllFoodOrders(){
        return service.getAll();
    }

}
